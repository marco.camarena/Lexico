#include "Scanner.h"

int salto = 0;
int ultimo = 0;
int pos = 0;
token Token;
int retorno;
std::string palabra;

token scanner(FILE *fichero){

    while(wsp(fichero));

    Token = comentario(fichero);
    if(Token != _no) return Token;

    Token = id(fichero);
    if(Token != _no) return Token;

    Token = numeros(fichero);
    if(Token != _no) return Token;

    Token = unique(fichero);
    if(Token != _no) return Token;

    if(eof(fichero)) return _eof;

    return _err;
}

bool wsp(FILE *fichero){
    if(isspace(siguiente(fichero))){
        acepta();
        return true;
    }
    falla();
    return false;
}

token id(FILE *fichero){
    palabra = "";
    int actual = 0, anterior;
    char c;

    while(actual != 3){
        anterior = actual;
        c = siguiente(fichero);

        if(c == '\n'){
                salto++;
        }
        switch(actual){
            case 0:
                if(c == '_' || isalpha(c)) actual = 1;
                else actual = 3;
            break;

            case 1:
                if(isalnum(c) || c == '_') actual = 1;
                else if(c == 39) actual = 2;
                else actual = 3;
            break;

            case 2:
                if(c == 39) actual = 2;
                else actual = 3;
            break;
        }
        if (actual != 3) palabra.push_back(c);
    }
    if(palabra == "identity"){
        retroceso();
        acepta();
        return _pal1;
    }else if(palabra == "transpose"){
        retroceso();
        acepta();
        return _pal2;
    }else if(palabra == "throw"){
        retroceso();
        acepta();
        return _pal3;
    }else if(anterior == 1 || anterior == 2){
        retroceso();
        acepta();
        return _id;
    }else{
        falla();
        return _no;
    }
}

token numeros(FILE *fichero){
    palabra = "";
    int actual = 0, anterior;
    char c;

    while(actual != 12 && actual != 13){
        anterior = actual;
        c = siguiente(fichero);

        if(c == '\n'){
        salto++;
        }
        switch(actual){
            case 0:
                if(c == '0') actual = 1;
                else if(c >= 49 && c <= 57) actual = 2;
                else actual = 13;
            break;

            case 1:
                if(c >= 48 && c <= 55) actual = 3;
                else if(c == 46) actual = 5;
                else if(c == 'x' || c == 'X') actual = 4;
                else if(isalpha(c)) actual = 12;
                else actual = 13;
            break;

            case 2:
                if(isdigit(c)) actual = 2;
                else if(c == 46) actual = 5;
                else if(c == 'e' || c == 'E') actual = 6;
                else if(isalpha(c)) actual = 12;
                else actual = 13;
            break;

            case 3:
                if(c >= 48 && c <= 55) actual = 3;
                else if(isalpha(c)) actual = 12;
                else actual = 13;
            break;

            case 4:
                if(isdigit(c)) actual = 7;
                else if(c >= 65 && c <= 70) actual = 7;
                else if(c >= 97 && c <= 102) actual = 7;
                else actual = 13;
            break;

            case 5:
                if(isdigit(c)) actual = 8;
                else actual = 13;
            break;

            case 6:
                if(isdigit(c)) actual = 9;
                else if(c == 43 || c == 45) actual = 10;
                else actual = 13;
            break;

            case 7:
                if(isdigit(c)) actual = 11;
                else if(c >= 65 && c <= 70) actual = 11;
                else if(c >= 97 && c <= 102) actual = 11;
                else actual = 13;
            break;

            case 8:
                if(isdigit(c)) actual = 8;
                else if(c == 'e' || c == 'E') actual = 6;
                else if(isalpha(c)) actual = 12;
                else actual = 13;
            break;

            case 9:
                if(isdigit(c)) actual = 9;
                else if(isalpha(c)) actual = 12;
                else actual = 13;
            break;

            case 10:
                if(isdigit(c)) actual = 9;
                else actual = 13;
            break;

            case 11:
                if(isdigit(c)) actual = 7;
                else if(c >= 65 && c <= 70) actual = 7;
                else if(c >= 97 && c <= 102) actual = 7;
                else if((c >= 71 && c <= 90) || (c >= 103 && c <= 122)) actual = 12;
                else actual = 13;
            break;
        }
        if(actual != 13 && actual != 12) palabra.push_back(c);
    }
    if(actual != 12){
        if(anterior == 1 || anterior == 3){
            retroceso();
            acepta();
            return _octal;
        }else if(anterior == 2 || anterior == 8 || anterior == 9){
            retroceso();
            acepta();
           return _real;
        }else if(anterior == 11){
            retroceso();
            acepta();
            return _hexa;
        }else{
            falla();
            return _no;
        }
    }else return _err;
}

token comentario(FILE *fichero){
    palabra = "";
    char c;

    c = siguiente(fichero);
    if(c == '!'){
        while(true){
            c = siguiente(fichero);
            if(c == '\n'){
                salto++;
                break;
            }
        }
        retroceso();
        acepta();
        return _comen;
    }else{
        falla();
        return _no;
    }
}

token unique(FILE *fichero){
    palabra = "";
    int actual = 0, anterior;
    char c;

    while(actual != 12){
        anterior = actual;
        c = siguiente(fichero);

        if(c == '\n'){
            salto++;
            break;
        }
        switch(actual){
            case 0:
                if(c == '(') {actual = 1; Token = _lpar;}
                else if(c == ')') {actual = 2; Token = _rpar;}
                else if(c == '[') {actual = 3; Token = _lcor;}
                else if(c == ']') {actual = 4; Token = _rcor;}
                else if(c == '+') {actual = 5; Token = _suma;}
                else if(c == '-') {actual = 6; Token = _resta;}
                else if(c == '*') {actual = 7; Token = _mult;}
                else if(c == '/') {actual = 8; Token = _div;}
                else if(c == ',') {actual = 9; Token = _coma;}
                else if(c == ';') {actual = 10; Token = _puncoma;}
                else if(c == ':') {actual = 11; Token = _dospunt;}
                else actual = 12;
            break;
            default:
                actual = 12;
            break;
        }
        if (actual != 12) palabra.push_back(c);
    }
    if(anterior != 12){
        retroceso();
        acepta();
        return Token;
    }else{
        falla();
        return _no;
    }
}

bool eof(FILE *fichero){
    if(siguiente(fichero) == EOF) return true;
    return false;
}

int saltos(){return salto;}

void acepta(){ultimo = pos;}

void falla(){pos = ultimo;}

char siguiente(FILE *fichero){
    char c;
    fseek(fichero, pos, SEEK_SET);
    c = fgetc(fichero);
    pos++;
    return c;
}

void retroceso(){pos--;}

void imprimir(){std::cout<<palabra;}
