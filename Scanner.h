#ifndef __Scanner_header_
#define __Scanner_haeder_

#include<cstdio>
#include<cctype>
#include<fstream>
#include<iostream>

typedef enum { _id, _comen, _pal1, _pal2, _pal3, _octal, _hexa, _real, _lpar, _rpar, _lcor, _rcor,
               _suma, _resta, _mult, _div, _coma, _puncoma, _dospunt, _eof, _err, _no} token;

token scanner(FILE *);
bool wsp(FILE *);
token id(FILE *);
token numeros(FILE *);
token comentario(FILE *);
token unique(FILE *);
bool eof(FILE *);

int saltos();
void acepta();
void falla();
char siguiente(FILE *);
void retroceso();
void imprimir();

#endif
