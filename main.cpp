#include "Scanner.h"

int main(){
    FILE *fichero;

    char nombre[15];

    printf("Nombre de Archivo: ");
    std::cin >> nombre;
    printf("\n");

    fichero = fopen(nombre, "r");

    token t;

    do{
        t = scanner(fichero);

        switch(t){

            case _id:   printf("\nIdentificador       \t"); break;
            case _pal1:  printf("\nIdentity           \t"); break;
            case _pal2:  printf("\nTranspose          \t"); break;
            case _pal3:  printf("\nThrow              \t"); break;
            case _octal:printf("\nOctal               \t"); break;
            case _hexa: printf("\nHexadecimal         \t"); break;
            case _real: printf("\nReal                \t"); break;
            case _lpar: printf("\nParentesis Izquierdo\t"); break;
            case _rpar: printf("\nParentesis Derecho  \t"); break;
            case _lcor: printf("\nCorchete Izquierdo  \t"); break;
            case _rcor: printf("\nCorchete Derecho    \t"); break;
            case _suma: printf("\nSuma                \t"); break;
            case _resta:printf("\nResta               \t"); break;
            case _mult: printf("\nMultiplicacion      \t"); break;
            case _div:  printf("\nDivision            \t"); break;
            case _coma:  printf("\nComa               \t"); break;
            case _puncoma:  printf("\nPunto y Coma    \t"); break;
            case _dospunt:  printf("\nDos Puntos      \t"); break;
            case _eof:  printf("\n\nFin de Archivo      \t"); break;
            case _err:  printf("\nError               \t"); break;
        }
        if(t != _no) imprimir();
    }while(t != _err && t != _eof);

    fclose(fichero);
    if(t == _err) printf("\n\nError en Linea %i\n\n", saltos()+1);
    else printf("\n\nAnalisis Terminado\nNumero de Lineas: %i\n\n", saltos());

    return 0;
}